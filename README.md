# `go-ebisusbay`

A package that wraps the Ebisus Bay API for use in Go applications.

# Usage

## Retrieving sales listings of a collection

```go
if collectionInstance, err := ebisusbay.GetCollection(ebisusbay.GetCollectionOpts{
  Address: "0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56",
}); err != nil {
  panic(err)
}
```

Example available at [./examples/collection](./examples/collection).

## Filtering sales listings of a collection by trait

```go
if collectionInstance, err := ebisusbay.GetCollection(ebisusbay.GetCollectionOpts{
  Address: "0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56",
  Traits: map[string][]string{
    "Background": {"Blue"},
    "Clothes":    {"Casual Shirt"},
  },
}); err != nil {
  panic(err)
}
```

Example available at [./examples/collection](./examples/collection).

## Retrieving all collections

```go
if collectionsInstance, err := ebisusbay.GetCollections(ebisusbay.GetCollectionsOpts{}); err != nil {
  panic(err)
}
```

Example available at [./examples/collections](./examples/collections).

## Retrieving statistics of a single collection

```go
if collectionsInstance, err := ebisusbay.GetCollections(ebisusbay.GetCollectionsOpts{
  Address: "0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56",
}); err != nil {
  panic(err)
}
```

Example available at [./examples/collection_stats](./examples/collection_stats).

## Retrieving recent sales of a single collection

```go
if salesInstance, err := ebisusbay.GetSales(ebisusbay.GetSalesOpts{
  Address: "0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56",
}); err != nil {
  panic(err)
}
```

Example available at [./examples/sales](./examples/sales).

## Retrieving rarity tables for a single collection

```go
if rarityInstance, err := ebisusbay.GetRarity("0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56"); err != nil {
  panic(err)
}
```

Example available at [./examples/rarity](./examples/rarity).

# Contribution

## Useful tools

1. [JSON-to-GO](https://transform.tools/json-to-go) - used to generate the Go structs from the API responses

## Other references

1. [Sample API requests](./docs/sample-api-requests.md)
