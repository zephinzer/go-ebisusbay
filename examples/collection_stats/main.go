package main

import (
	"bytes"
	"fmt"

	"github.com/olekukonko/tablewriter"
	"github.com/zephinzer/go-ebisusbay"
)

func main() {
	GetCollections()
}

func GetCollections() {
	collectionsInstance, err := ebisusbay.GetCollections(ebisusbay.GetCollectionsOpts{
		Address: "0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56",
	})
	if err != nil {
		panic(err)
	}

	collections := bytes.NewBuffer(nil)
	collectionsTable := tablewriter.NewWriter(collections)
	collectionsTable.SetHeader([]string{
		"collection name",
		"floor price",
		"total volume",
		"active listings",
	})
	for _, collection := range collectionsInstance.Collections {
		collectionsTable.Append([]string{
			collection.Collection,
			collection.FloorPrice,
			collection.TotalVolume,
			collection.NumberActive,
		})
	}
	collectionsTable.Render()
	fmt.Println(collections)
}
