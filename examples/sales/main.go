package main

import (
	"bytes"
	"fmt"
	"time"

	"github.com/olekukonko/tablewriter"
	"github.com/zephinzer/go-ebisusbay"
)

func main() {
	GetMadMeerkatsSales()
}

func GetMadMeerkatsSales() {
	salesInstance, err := ebisusbay.GetSales(ebisusbay.GetSalesOpts{
		Address: "0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56",
	})
	if err != nil {
		panic(err)
	}

	salesListings := bytes.NewBuffer(nil)
	salesListingsTable := tablewriter.NewWriter(salesListings)
	salesListingsTable.SetHeader([]string{
		"id",
		"price",
		"sold to",
		"sold by",
		"at",
	})
	for _, listing := range salesInstance.Listings {
		salesListingsTable.Append([]string{
			listing.Nft.NftID,
			listing.Price,
			listing.Purchaser[:6] + "..." + listing.Purchaser[len(listing.Purchaser)-4:],
			listing.Seller[:6] + "..." + listing.Seller[len(listing.Seller)-4:],
			time.Unix(int64(listing.SaleTime), 0).Format(time.RFC1123),
		})
	}

	salesListingsTable.Render()
	fmt.Println(salesListings)
}
