package main

import (
	"bytes"
	"fmt"
	"strconv"

	"github.com/olekukonko/tablewriter"
	"github.com/zephinzer/go-ebisusbay"
)

func main() {
	GetMadMeerkatsFloor()
}

func GetMadMeerkatsFloor() {
	collectionInstance, err := ebisusbay.GetCollection(ebisusbay.GetCollectionOpts{
		Address: "0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56",
		Traits: map[string][]string{
			"Background": {"Blue"},
			"Clothes":    {"Casual Shirt"},
		},
	})
	if err != nil {
		panic(err)
	}

	listings := bytes.NewBuffer(nil)
	listingsTable := tablewriter.NewWriter(listings)
	listingsTable.SetHeader([]string{
		"token id", "rank", "price", "link",
	})
	for _, nft := range collectionInstance.Nfts {
		price := "-"
		if nft.Market.Price != "" {
			price = nft.Market.Price + " CRO"
		}
		listingsTable.Append([]string{
			nft.ID,
			strconv.Itoa(nft.Rank),
			price,
			nft.Market.URI,
		})
	}
	listingsTable.Render()
	fmt.Println(listings)
}
