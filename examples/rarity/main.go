package main

import (
	"bytes"
	"fmt"
	"strconv"

	"github.com/olekukonko/tablewriter"
	"github.com/zephinzer/go-ebisusbay"
)

func main() {
	GetMadMeerkatsRarity()
}

func GetMadMeerkatsRarity() {
	rarityInstance, err := ebisusbay.GetRarity("0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56")
	if err != nil {
		panic(err)
	}

	traitsRarity := bytes.NewBuffer(nil)
	traitsRarityTable := tablewriter.NewWriter(traitsRarity)
	traitsRarityTable.SetHeader([]string{
		"category",
		"trait",
		"count",
		"occurrence",
	})
	for category, trait := range *rarityInstance {
		traitsRarityTable.Append([]string{
			category,
			"---",
			"---",
			"---",
		})
		for traitLabel, traitDetails := range trait {
			traitsRarityTable.Append([]string{
				">",
				traitLabel,
				strconv.Itoa(traitDetails.Count),
				strconv.FormatFloat(traitDetails.Occurrence*100, 'f', 2, 64) + "%",
			})
		}
	}
	traitsRarityTable.Render()
	fmt.Println(traitsRarity)
}
