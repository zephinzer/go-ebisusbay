package ebisusbay

type Attributes []Attribute

type Attribute struct {
	TraitType  string  `json:"trait_type"`
	Value      string  `json:"value"`
	Occurrence float64 `json:"occurrence"`
}

type Collection struct {
	Nfts        Nfts                         `json:"nfts"`
	Summary     CollectionSummary            `json:"summary"`
	TraitRarity map[string]map[string]Rarity `json:"traitRarity"`
}

type CollectionSummariesResponse struct {
	Status      int                 `json:"status"`
	Collections CollectionSummaries `json:"collections"`
}

type CollectionResponse struct {
	Status     int  `json:"status"`
	Page       int  `json:"page"`
	TotalCount int  `json:"totalCount"`
	PageSize   int  `json:"pageSize"`
	TotalPages int  `json:"totalPages"`
	Nfts       Nfts `json:"nfts"`
}

type CollectionsResponse struct {
	Status      int                 `json:"status"`
	Collections CollectionSummaries `json:"collections"`
}

type CollectionSummaries []CollectionSummary

type CollectionSummary struct {
	Sales1D          string `json:"sales1d"`
	Sales7D          string `json:"sales7d"`
	Sales30D         string `json:"sales30d"`
	Volume1D         string `json:"volume1d"`
	Volume7D         string `json:"volume7d"`
	TotalFees        string `json:"totalFees"`
	Volume30D        string `json:"volume30d"`
	Collection       string `json:"collection"`
	FloorPrice       string `json:"floorPrice"`
	TotalVolume      string `json:"totalVolume"`
	NumberActive     string `json:"numberActive"`
	NumberOfSales    string `json:"numberOfSales"`
	TotalRoyalties   string `json:"totalRoyalties"`
	AverageSalePrice string `json:"averageSalePrice"`
}

type Nfts []Nft

type Nft struct {
	Market        NftMarket   `json:"market"`
	ID            string      `json:"id"`
	Address       string      `json:"address"`
	Edition       int         `json:"edition"`
	Name          string      `json:"name"`
	Image         string      `json:"image"`
	OriginalImage string      `json:"original_image"`
	Description   string      `json:"description"`
	Score         float64     `json:"score"`
	Rank          int         `json:"rank"`
	Attributes    Attributes  `json:"attributes"`
	Slug          interface{} `json:"slug"`
}

type NftMarket struct {
	ID    int    `json:"id"`
	Price string `json:"price"`
	URI   string `json:"uri"`
}

type RarityResponse map[string]map[string]Rarity

type Rarity struct {
	Count      int     `json:"count"`
	Occurrence float64 `json:"occurrence"`
}

type SalesResponse struct {
	Status     int           `json:"status"`
	Page       int           `json:"page"`
	TotalCount int           `json:"totalCount"`
	PageSize   int           `json:"pageSize"`
	TotalPages int           `json:"totalPages"`
	Listings   SalesListings `json:"listings"`
}

type SalesListings []SalesListing

type SalesListing struct {
	ListingID   int             `json:"listingId"`
	Is1155      bool            `json:"is1155"`
	Invalid     int             `json:"invalid"`
	Valid       bool            `json:"valid"`
	Seller      string          `json:"seller"`
	Purchaser   string          `json:"purchaser"`
	State       int             `json:"state"`
	Price       string          `json:"price"`
	Royalty     string          `json:"royalty"`
	Fee         string          `json:"fee"`
	SaleTime    int             `json:"saleTime"`
	ListingTime int             `json:"listingTime"`
	NftID       string          `json:"nftId"`
	NftAddress  string          `json:"nftAddress"`
	Nft         SalesListingNft `json:"nft"`
}

type SalesListingNft struct {
	NftID         string     `json:"nftId"`
	NftAddress    string     `json:"nftAddress"`
	Edition       int        `json:"edition"`
	Name          string     `json:"name"`
	Image         string     `json:"image"`
	OriginalImage string     `json:"original_image"`
	Description   string     `json:"description"`
	Rank          int        `json:"rank"`
	Attributes    Attributes `json:"attributes"`
}

type Traits map[string][]string
