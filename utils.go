package ebisusbay

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func sendQueryRequest(targetUrl string, responseStructurePointer interface{}) error {
	req, err := http.NewRequest(http.MethodGet, targetUrl, nil)
	if err != nil {
		return fmt.Errorf("failed to create http request: %s", err)
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("failed to perform http request: %s", err)
	}
	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("failed to read response body: %s", err)
	}
	if err := json.Unmarshal(resBody, responseStructurePointer); err != nil {
		return fmt.Errorf("failed to unmarshal response body into a collection response: %s", err)
	}

	return nil
}
