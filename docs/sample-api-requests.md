# API

## All collections

```sh
curl 'https://api.ebisusbay.com//collections?sortBy=totalVolume&direction=desc' \
  -H 'authority: api.ebisusbay.com' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="90"' \
  -H 'dnt: 1' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36' \
  -H 'accept: */*' \
  -H 'origin: https://app.ebisusbay.com' \
  -H 'sec-fetch-site: same-site' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://app.ebisusbay.com/' \
  -H 'accept-language: en-US,en;q=0.9' \
  -H 'sec-gpc: 1' \
  -H 'if-none-match: "115c9-cYLY3Q5DqnlvzTYUFe08pOKFQq0"' \
  --compressed
```

## Main collection page

Canonical API request recorded on 23rd May 2022:

```sh
curl 'https://api.ebisusbay.com/fullcollections?page=1&pageSize=50&sortBy=price&direction=asc&address=0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56' \
  -H 'authority: api.ebisusbay.com' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="90"' \
  -H 'dnt: 1' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36' \
  -H 'accept: */*' \
  -H 'origin: https://app.ebisusbay.com' \
  -H 'sec-fetch-site: same-site' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://app.ebisusbay.com/' \
  -H 'accept-language: en-US,en;q=0.9' \
  -H 'sec-gpc: 1' \
  -H 'if-none-match: "12edb-GekEqPPEGKOi/F9MqkmHm+hdrII"' \
  --compressed
```

## Rankings sort

Canonical API request recorded on 23rd May 2022:

```sh
curl 'https://api.ebisusbay.com/fullcollections?page=1&pageSize=50&sortBy=rank&direction=desc&address=0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56&traits=%7B%7D' \
  -H 'authority: api.ebisusbay.com' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="90"' \
  -H 'dnt: 1' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36' \
  -H 'accept: */*' \
  -H 'origin: https://app.ebisusbay.com' \
  -H 'sec-fetch-site: same-site' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://app.ebisusbay.com/' \
  -H 'accept-language: en-US,en;q=0.9' \
  -H 'sec-gpc: 1' \
  --compressed
```

## Latest listings sort

Canonical API request recorded on 23rd May 2022:

```sh
curl 'https://api.ebisusbay.com/fullcollections?page=1&pageSize=50&sortBy=listingId&direction=desc&address=0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56&traits=%7B%7D' \
  -H 'authority: api.ebisusbay.com' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="90"' \
  -H 'dnt: 1' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36' \
  -H 'accept: */*' \
  -H 'origin: https://app.ebisusbay.com' \
  -H 'sec-fetch-site: same-site' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://app.ebisusbay.com/' \
  -H 'accept-language: en-US,en;q=0.9' \
  -H 'sec-gpc: 1' \
  --compressed
```

## Filtering by min price

Canonical API request recorded on 23rd May 2022:

```sh
curl 'https://api.ebisusbay.com/fullcollections?page=1&pageSize=50&sortBy=rank&direction=desc&address=0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56&traits=%7B%7D&minPrice=1000' \
  -H 'authority: api.ebisusbay.com' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="90"' \
  -H 'dnt: 1' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36' \
  -H 'accept: */*' \
  -H 'origin: https://app.ebisusbay.com' \
  -H 'sec-fetch-site: same-site' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://app.ebisusbay.com/' \
  -H 'accept-language: en-US,en;q=0.9' \
  -H 'sec-gpc: 1' \
  --compressed
```

## Filtering by max price

Canonical API request recorded on 23rd May 2022:

```sh
curl 'https://api.ebisusbay.com/fullcollections?page=1&pageSize=50&sortBy=rank&direction=desc&address=0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56&traits=%7B%7D&maxPrice=5000' \
  -H 'authority: api.ebisusbay.com' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="90"' \
  -H 'dnt: 1' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36' \
  -H 'accept: */*' \
  -H 'origin: https://app.ebisusbay.com' \
  -H 'sec-fetch-site: same-site' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://app.ebisusbay.com/' \
  -H 'accept-language: en-US,en;q=0.9' \
  -H 'sec-gpc: 1' \
  --compressed
```

## Filtering by trait

The following includes a filter for the trait `"Background"` to equal `"Orange"`:

```sh
curl 'https://api.ebisusbay.com/fullcollections?page=1&pageSize=50&sortBy=price&direction=asc&address=0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56&traits=%7B%22Background%22%3A%5B%22Orange%22%5D%7D' \
  -H 'authority: api.ebisusbay.com' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="90"' \
  -H 'dnt: 1' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36' \
  -H 'accept: */*' \
  -H 'origin: https://app.ebisusbay.com' \
  -H 'sec-fetch-site: same-site' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://app.ebisusbay.com/' \
  -H 'accept-language: en-US,en;q=0.9' \
  -H 'sec-gpc: 1' \
  --compressed
```

The following is a filter for two traits:

```sh
curl 'https://api.ebisusbay.com/fullcollections?page=1&pageSize=50&sortBy=id&direction=desc&address=0x89dBC8Bd9a6037Cbd6EC66C4bF4189c9747B1C56&traits=%7B%22Background%22%3A%5B%22Blue%22%5D%2C%22Clothes%22%3A%5B%22Casual+Shirt%22%5D%7D' \
  -H 'authority: api.ebisusbay.com' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="90"' \
  -H 'dnt: 1' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36' \
  -H 'accept: */*' \
  -H 'origin: https://app.ebisusbay.com' \
  -H 'sec-fetch-site: same-site' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://app.ebisusbay.com/' \
  -H 'accept-language: en-US,en;q=0.9' \
  -H 'sec-gpc: 1' \
  --compressed
```

## Rarity

```sh
curl 'https://app.ebisusbay.com/files/0xdc5bbdb4a4b051bdb85b959eb3cbd1c8c0d0c105/rarity.json' \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="90"' \
  -H 'Referer: https://app.ebisusbay.com/collection/mm-treehouse' \
  -H 'DNT: 1' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36' \
  --compressed
```

## Listings

```sh
curl 'https://api.ebisusbay.com/listings?state=1&page=1&pageSize=50&sortBy=saleTime&direction=desc&collection=0xDC5bBDb4A4b051BDB85B959eB3cBD1c8C0d0c105' \
  -H 'authority: api.ebisusbay.com' \
  -H 'sec-ch-ua: "Chromium";v="93", " Not;A Brand";v="99"' \
  -H 'dnt: 1' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36' \
  -H 'sec-ch-ua-platform: "Linux"' \
  -H 'accept: */*' \
  -H 'origin: https://app.ebisusbay.com' \
  -H 'sec-fetch-site: same-site' \
  -H 'sec-fetch-mode: cors' \
  -H 'sec-fetch-dest: empty' \
  -H 'referer: https://app.ebisusbay.com/' \
  -H 'accept-language: en-US,en;q=0.9,es-ES;q=0.8,es;q=0.7,en-GB;q=0.6' \
  -H 'sec-gpc: 1' \
  --compressed
```
