package ebisusbay

type Direction string
type SortKey string

const (
	ApiBaseUrl = "https://api.ebisusbay.com"
	AppBaseUrl = "https://app.ebisusbay.com"

	DirectionAscending  Direction = "asc"
	DirectionDescending Direction = "desc"

	FilenameRarity = "rarity.json"

	PathCollection  = "/fullcollections"
	PathCollections = "/collections"
	PathFiles       = "/files"
	PathListings    = "/listings"

	QueryKeyAddress    = "address"
	QueryKeyCollection = "collection"
	QueryKeyDirection  = "direction"
	QueryKeyPage       = "page"
	QueryKeyPageSize   = "pageSize"
	QueryKeySortBy     = "sortBy"
	QueryKeyState      = "state"
	QueryKeyTraits     = "traits"

	SortKeyTotalVolume SortKey = "totalVolume"
	SortKeyPrice       SortKey = "price"
	SortKeyRank        SortKey = "rank"
	SortKeySaleTime    SortKey = "saleTime"
	SortKeyTokenID     SortKey = "id"

	StateSold = "1"
)
